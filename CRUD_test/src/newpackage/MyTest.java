package newpackage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class MyTest 
{
public static void main(String[] args) {
	
	String exePath = "D:\\Users\\703217907\\Documents\\chromedriver.exe";
	System.setProperty("webdriver.chrome.driver", exePath);
		
		// Create a new instance of the Firefox driver
		WebDriver driver = new ChromeDriver();
		
        //Launch the Online Store Website
		driver.get("http://35.174.137.193:9090/demo/#/");
 
        // Print a Log In message to the screen
        System.out.println("Successfully opened the demo App");
 
		//Wait for 5 Sec
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (!driver.findElement(By.id("uname")).getText().isEmpty())
			System.out.println("Username feild  beshould blank by default");
		if (!driver.findElement(By.id("age")).getText().isEmpty())
			System.out.println("Age feild should be blank by default");
		if (!driver.findElement(By.id("salary")).getText().isEmpty())
			System.out.println("Salary feild should be blank by default");
		if (!driver.findElement(By.xpath("//button[text()='Reset']")).isEnabled())
			System.out.println("reset button should be enabled by default");
		if (!driver.findElement(By.xpath("//input[@value='Add']")).isDisplayed())
			System.out.println("Add button should be displayed by default");
		JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', arguments[1]);", By.xpath("//input[@value='Add']"), "color: yellow; border: 2px solid yellow;");
        js.executeScript("arguments[0].setAttribute('style', arguments[1]);", By.xpath("//input[@value='Add']"), "");
        
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//driver.wait("5000");
        // Close the driver
        driver.quit();

}
}
